<?php

namespace Clavius\Pipes\Repo;

interface StoreRepoInterface extends NullRepoInterface
{
    public function load(string $relativePath): mixed;

    public function store(string $relativePath, mixed $data): bool;

    public function delete(string $relativePath);

    public function setRoot(string $path);
}
