<?php

namespace Clavius\Pipes\Services;

class Helper
{
    /**
     * Safe append array to array distinct elements.
     */
    public static function append($ar1, $ar2): array
    {
        foreach ($ar2 as $item) {
            if (!in_array($item, $ar1)) {
                $ar1[] = $item;
            }
        }

        return $ar1;
    }

    public static function add($ar1, $ar2): array
    {
        foreach ($ar2 as $item) {
            $ar1[] = $item;
        }

        return $ar1;
    }

    public static function fileForceContents($path, $contents): bool|int
    {
        $path = str_replace('/', DIRECTORY_SEPARATOR, $path);
        $parts = explode(DIRECTORY_SEPARATOR, $path);
        $file = array_pop($parts);

        $dir = implode(DIRECTORY_SEPARATOR, $parts);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        return file_put_contents("$dir/$file", $contents);
    }

    public static function shortClassName(string $classNameWithNamespace): string
    {
        return substr($classNameWithNamespace, strrpos($classNameWithNamespace, '\\') + 1);
    }
}
