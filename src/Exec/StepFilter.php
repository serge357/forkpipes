<?php

namespace Clavius\Pipes\Exec;

use Clavius\Pipes\Data\Vault;

class StepFilter implements PipeStep
{
    public function __construct(protected PipeSelector $selector, protected readonly string $stepName, protected readonly ?string $srcStepName = null)
    {
    }

    public function config(string|array $config): void
    {
        $this->selector->config($config);
    }

    public function do(Vault $vault): self
    {
        $srcStepName = $this->srcStepName ?: $vault->getPrevStepName();
        $stepElements = $vault->getStep($srcStepName);

        foreach ($stepElements as $descriptor) {
            $value = $vault->getDescriptorValue($descriptor, $srcStepName);

            if ($this->selector->check($value)) {
                $vault->putValue($this->stepName, $value, $descriptor);
            }
        }

        return $this;
    }
}
