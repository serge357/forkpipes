<?php

namespace Clavius\Pipes\Exec;

interface PipeConverter
{
    public function convert($source): array;

    public function setConfig(array|string $config);
}
