<?php

namespace Clavius\Pipes\Pipelines;

use Clavius\Pipes\Converters\TextExplode;
use Clavius\Pipes\Converters\TextsImplode;
use Clavius\Pipes\Exec\Pipeline;
use Clavius\Pipes\Pipe;

class TextDullPipeline extends Pipeline
{
    public static function addConverters(Pipe $pipe, array $settings = []): void
    {
        $pipe->addConverter('prg', new TextExplode(), "\n")->addConverter('snt', new TextExplode(), '.')->addConverter('wrd', new TextExplode(), '_');
        $pipe->addCombineConverter('snt2', 'snt', new TextsImplode(), ' ')->addCombineConverter('prg2', 'prg', new TextsImplode(), '.')->addCombineConverter('fin', 'start', new TextsImplode(), "\n");
    }
}
