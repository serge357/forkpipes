<?php

namespace Clavius\Pipes;

use Clavius\Pipes\Data\Vault;
use Clavius\Pipes\Exec\Combiner;
use Clavius\Pipes\Exec\PipeConverter;
use Clavius\Pipes\Exec\PipeSelector;
use Clavius\Pipes\Exec\PipeStep;
use Clavius\Pipes\Exec\StepFilter;
use Clavius\Pipes\Exec\Stepper;
use Clavius\Pipes\Ports\Ports;

class Pipe
{
    /**
     * @var PipeStep[] array
     */
    protected array $steppers = [];

    public const START_STEP = 'start';

    public function __construct(public Ports $ports)
    {
    }

    public function do(mixed $start): array
    {
        $vault = $this->exec($start);

        return $this->exec($start)->getStepValues($vault->getLastStepName());
    }

    public function exec(mixed $start): Vault
    {
        $vault = new Vault($this->ports->getDepo());
        $this->startProcess($vault, $start);

        foreach ($this->steppers as $stepName => $stepper) {
            $vault->addStep($stepName);
            $stepper->do($vault);
        }

        return $vault;
    }

    public function startProcess(Vault $vault, mixed $startValue): void
    {
        $vault->addStep(self::START_STEP);
        $vault->putValue(self::START_STEP, $startValue, null);
    }

    public function addStep($stepName, PipeStep $stepper, $config = null): self
    {
        if ($config) {
            $stepper->config($config);
        }

        $this->steppers[$stepName] = $stepper;

        return $this;
    }

    public function addConverter($stepName, PipeConverter $converter, $config = null): self
    {
        $stepper = new Stepper($converter, $stepName);

        return $this->addStep($stepName, $stepper, $config);
    }

    public function addFilter($stepName, PipeSelector $selector, $config = null, $srcStepName = null): self
    {
        $stepper = new StepFilter($selector, $stepName, $srcStepName);

        return $this->addStep($stepName, $stepper, $config);
    }

    public function addCombine($stepName, $parentStepName, $config = null, $srcStepName = null): self
    {
        $stepper = new Combiner($stepName, $parentStepName, $srcStepName);

        return $this->addStep($stepName, $stepper, $config);
    }

    public function addCombineConverter($stepName, $parentStepName, PipeConverter $converter, $config = null): self
    {
        $this->addCombine($stepName, $parentStepName, $config);
        $this->addConverter($stepName.'+', $converter, $config);

        return $this;
    }
}
