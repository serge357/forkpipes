<?php

namespace Clavius\Pipes\Exec;

interface PipeSelector
{
    public function check($source): bool;

    public function config(array|string $config);
}
