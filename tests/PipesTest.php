<?php

namespace Clavius\Pipes\Tests;

use Clavius\Pipes\Pipe;
use Clavius\Pipes\Pipelines\TextDullPipeline;
use Clavius\Pipes\Ports\PortsDepo;
use Clavius\Pipes\Repo\ArrayRepo;
use Clavius\Pipes\Services\FakeGenerator;
use PHPUnit\Framework\TestCase;


class PipesTest extends TestCase
{
    public function testExplodes()
    {
        $gen = new FakeGenerator();
        $text = $gen->simple(100000)."\n".$gen->simple(120000)."\n".$gen->simple(140000);

        $ports = new PortsDepo(new ArrayRepo());

        $pipe = new Pipe($ports);

        TextDullPipeline::addConverters($pipe);

        $res = $pipe->do($text);

        self::assertNotEmpty($text);
        self::assertEquals($text, current($res));
    }
}
