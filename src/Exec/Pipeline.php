<?php

namespace Clavius\Pipes\Exec;

use Clavius\Pipes\Pipe;
use Clavius\Pipes\Ports\Ports;

abstract class Pipeline
{
    public function do(mixed $data, Ports $ports, array $configuration = []): array
    {
        $pipe = new Pipe($ports);
        $this->addConverters($pipe, $configuration);

        return $pipe->do($data);
    }

    abstract public static function addConverters(Pipe $pipe, array $configuration = []): void;
}
