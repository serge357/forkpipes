<?php

namespace Clavius\Pipes\Exec;

use Clavius\Pipes\Data\Pitchfork;
use Clavius\Pipes\Data\Vault;

class Combiner implements PipeStep
{
    public function __construct(protected readonly string $stepName, protected $parentStepName, protected readonly ?string $srcStepName = null)
    {
    }

    public function config(string|array $config): void
    {
    }

    public function do(Vault $vault): self
    {
        Pitchfork::combine($vault, $this->srcStepName ?: $vault->getPrevStepName(), $this->parentStepName, $this->stepName);

        return $this;
    }
}
