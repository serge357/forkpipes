<?php

namespace Clavius\Pipes\Exec;

use Clavius\Pipes\Data\Vault;

interface PipeStep
{
    public function do(Vault $vault): self;
}
