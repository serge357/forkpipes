<?php

namespace Clavius\Pipes\Data;

use Clavius\Pipes\Repo\StoreRepoInterface;

class Vault
{
    protected int $counter;
    protected array $steps;

    public function __construct(protected StoreRepoInterface $repo)
    {
        $this->counter = 0;
    }

    /** @return  Descriptor[] */
    public function getStep(string $name): array
    {
        return $this->steps[$name];
    }

    public function getDescriptor(string $stepName, $elementId): Descriptor
    {
        return $this->steps[$stepName][$elementId];
    }

    public function getStepValues(string $stepName): array
    {
        $elementDescriptors = $this->getStep($stepName);

        foreach ($elementDescriptors as $descriptor) {
            $res[] = $this->getDescriptorValue($descriptor, $stepName);
        }

        return $res ?? [];
    }

    public function addStep(string $name): void
    {
        $this->steps[$name] = [];
    }

    public function getStepsNames(): array
    {
        return array_keys($this->steps);
    }

    public function getLastStepName(): string
    {
        return array_key_last($this->steps);
    }

    public function getPrevStepName(): string
    {
        $steps = $this->getStepsNames();
        array_pop($steps);

        return end($steps);
    }

    public function getDest($id): mixed
    {
        return $this->repo->load($id);
    }

    public function getDescriptorValue(Descriptor $descriptor, string $stepName): mixed
    {
        return $this->getDest($descriptor->getStep($stepName));
    }

    protected function setDest($id, $value): void
    {
        $this->repo->store($id, $value);
    }

    protected function takeCounter(): int
    {
        return ++$this->counter;
    }

    protected function addToStep(string $name, Descriptor $value, string $elementId): void
    {
        $this->steps[$name][$elementId] = $value;
    }

    public function putValue(string $stepName, mixed $value, ?Descriptor $parentDescriptor): void
    {
        $newId = $this->takeCounter();
        $newDescriptor = Descriptor::make($newId, $stepName, $parentDescriptor);
        $this->setDest($newId, $value);
        $this->addToStep($stepName, $newDescriptor, $newId);
    }
}
