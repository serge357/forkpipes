<?php

namespace Clavius\Pipes\Ports;

use Clavius\Pipes\Repo\StoreRepoInterface;

class PortsDepo extends Ports
{
    public function __construct(protected StoreRepoInterface $depo)
    {
    }

    public function getDepo()
    {
        return $this->depo;
    }

    public static function setPorts($serviceList, array $configuration): PortsDepo
    {
        $selfSettings = $configuration[self::PORTS_SECTION];
        $depo = $serviceList->make($selfSettings->get('depo'));

        return new PortsDepo($depo);
    }
}
