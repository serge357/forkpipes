<?php

namespace Clavius\Pipes\Exec;

use Clavius\Pipes\Data\Pitchfork;
use Clavius\Pipes\Data\Vault;

class Stepper implements PipeStep
{
    public function __construct(protected readonly PipeConverter $converter, protected readonly string $stepName, protected readonly ?string $srcStepName = null)
    {
    }

    public function config(string|array $config): void
    {
        $this->converter->setConfig($config);
    }

    public function do(Vault $vault): self
    {
        $srcStepName = $this->srcStepName ?: $vault->getPrevStepName();
        $descriptors = $vault->getStep($srcStepName);

        foreach ($descriptors ?? [] as $descriptor) {
            $data = $vault->getDest($descriptor->getLast());
            $result = $this->converter->convert($data);
            $vault = Pitchfork::fork($vault, $this->stepName, $descriptor, $result);
        }

        return $this;
    }
}
