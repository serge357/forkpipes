<?php

namespace Clavius\Pipes\Services;

use Faker\Factory;
use Faker\Generator;
use Faker\Provider\en_US\Text;

class FakeGenerator
{
    protected Generator $faker;
    public int $fileCounter = 0;
    public int $textCounter = 0;

    public function __construct()
    {
        $this->faker = Factory::create();
        $this->faker->addProvider(new Text($this->faker));
    }

    /**
     * Random name.
     */
    public function name(): string
    {
        return $this->faker->domainWord();
    }

    /**
     * Random text.
     */
    public function text(int $length): string
    {
        return $this->faker->realTextBetween($length * 16, $length * 32);
    }

    /**
     * Random text.
     */
    public function simple(int $length): string
    {
        return $this->faker->text($length);
    }

    /**
     * Random list names.
     *
     * @return string[]
     */
    public function listNames(int $count, int $baseCount = 0): array
    {
        $bc = min($baseCount, $count);
        $ct = max($baseCount, $count);
        $times = mt_rand($bc, $ct);

        $list = [];
        for ($i = 0; $i < $times; ++$i) {
            $list[] = $this->name();
        }

        return $list;
    }

    /**
     * Random folder structure (nested arrays).
     */
    public function folders(int $count, int $depth, int $baseCount = 0): array
    {
        if ($depth <= 0) {
            return [];
        }
        $list = $this->listNames($count, $baseCount);
        $folder = [];
        foreach ($list as $item) {
            $folder[$item] = $this->folders($count, $depth - 1, $baseCount);
        }

        return $folder;
    }

    /**
     * List folder paths for generated folders structure (nested arrays).
     *
     * @return string[]
     */
    public function allFolders(array $folders, array $result = [], string $pathTo = ''): array
    {
        if (empty($folders)) {
            $result[] = $pathTo;

            return $result;
        }

        foreach ($folders as $name => $folder) {
            $newPath = $pathTo.DIRECTORY_SEPARATOR.$name;
            $result[] = $newPath;
            $add = $this->allFolders($folder, $result, $newPath);
            $result = Helper::append($result, $add);
        }

        return $result;
    }

    public function makeFolders(int $count, int $depth, int $baseCount = 0): array
    {
        $folders = $this->folders($count, $depth, $baseCount);
        $all = $this->allFolders($folders);

        return array_unique($all);
    }

    /**
     * Text of random MD file generated by pattern array
     * pattern array structure: patterns array
     * patterns available: 'hD' - header of D level, 'pD' - paragraph with D sentences.
     */
    public function randomMD(array $pattern = ['p']): string
    {
        $res = '';

        foreach ($pattern as $item) {
            if (is_string($item) && 'h' === $item[0]) {
                $length = mt_rand(5, 8);
                $sentence = $this->faker->realText($length * 32);
                $headerType = (int) substr($item, 1);
                $prefix = str_repeat('#', $headerType);
                $res .= $prefix.' '.$sentence."\n";
            }

            if (is_string($item) && 'p' === $item[0]) {
                $paragraphLen = (int) substr($item, 1);
                $paragraph = $this->faker->realText($paragraphLen * 256, 4);
                $res .= "\n".$paragraph."\n";
            }

            if (is_string($item) && 't' === $item[0]) {
                $strLen = (int) substr($item, 1);
                $sentence = $this->faker->realText($strLen * 16);
                $res .= $sentence;
            }
        }

        $this->textCounter += strlen($res);
        ++$this->fileCounter;

        return $res;
    }

    /**
     * Text for random MD field files generated by pattern arrays
     * field name is the patterns array key.
     */
    public function randomMDFields(array $patterns): array
    {
        $fields = [];

        foreach ($patterns as $name => $pattern) {
            $fields[$name] = $this->randomMD($pattern);
        }

        return $fields;
    }
}
