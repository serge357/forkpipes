<?php

namespace Clavius\Pipes\Converters;

use Clavius\Pipes\Exec\PipeConverter;

class TextExplode implements PipeConverter
{
    public string|array $config = [];

    public function setConfig(string|array $config): void
    {
        $this->config = $config;
    }

    public function convert($source): array
    {
        return explode("\n", $source);
    }
}
