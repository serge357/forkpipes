# Description #

### What does the package do ###

* The library is designed to support the forkpipes design pattern that I came up with
* Version 1.0

The pattern is used when it is necessary to divide data processing into several streams, at the next processing steps divide them into several more streams, with arbitrary nesting, and then combine these data streams. You can combine them either in an arbitrary way of grouping, or - simpler - in accordance with the initial division.

### Installation ###

* install package: composer require clavius/forkpipes
* Configuration: not required
* dependencies: for development and testing - phpunit, faker
* Database - not required
* Tests: php.exe vendor/phpunit/phpunit/phpunit --no-configuration tests

### Usage ###

- write classes with data processing functions - converters:
  - each converter implements the PipeConverter interface
  - accepts and returns data in the format of an array of elements
  - array elements can be any
- create a class inheriting from Clavius\Pipes\Exec\Pipeline, and assemble a pipeline in it from the converters for processing:
  - the class must implement the addConverters method
  - the pipeline steps are defined in the method
  - steps can be either converters, combinators, or filters
  - combinators simply combine the data collected in previous steps, filters select part of the data for the next step
  - an example of creating a pipeline - \Clavius\Pipes\Pipelines\TextDullPipeline
- create a depo that will be used by the pipeline class for intermediate storage of processing results
- create a pipeline class object (with depot) and call the addConverters and do methods with this object
- see example in the \Clavius\Pipes\Tests\PipesTest test

### Contacts ###

* the author of the project is Sergey Pavlov (clavius@gmail.com )