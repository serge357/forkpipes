<?php

namespace Clavius\Pipes\Converters;

use Clavius\Pipes\Exec\PipeConverter;

class TextsImplode implements PipeConverter
{
    public string|array $config = [];

    public function setConfig(string|array $config): void
    {
        $this->config = $config;
    }

    public function convert($source): array
    {
        return [implode($this->config, $source)];
    }
}
