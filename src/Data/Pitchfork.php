<?php

namespace Clavius\Pipes\Data;

class Pitchfork
{
    /**
     *  массив результатов (полученных в шаге) кладется в Vault (добавляется в текущий шаг).
     */
    public static function fork(Vault $vault, string $stepName, Descriptor $parentDescriptor, array $results): Vault
    {
        foreach ($results as $item) {
            $vault->putValue($stepName, $item, $parentDescriptor);
        }

        return $vault;
    }

    /**
     * элементы шага группируются по родительскому шагу в массив массивов для использования конвертером
     */
    public static function pitch(Vault $vault, string $stepName, string $parentStepName): array
    {
        $stepElements = $vault->getStep($stepName);
        $matrix = [];

        foreach ($stepElements as $descriptor) {
            $matrix[$descriptor->getStep($parentStepName)][] = $vault->getDescriptorValue($descriptor, $stepName);
        }

        return $matrix;
    }

    public static function combine(Vault $vault, string $srcStepName, string $parentStepName, string $newStepName): Vault
    {
        $matrix = self::pitch($vault, $srcStepName, $parentStepName);
        foreach ($matrix as $elementId => $item) {
            $vault->putValue($newStepName, $item, $vault->getDescriptor($parentStepName, $elementId));
        }

        return $vault;
    }
}
