<?php

namespace Clavius\Pipes\Data;

class Descriptor
{
    /**
     * [stepName => elementId].
     */
    public function __construct(protected array $chain)
    {
    }

    public static function make(string $elementId, string $stepName, ?Descriptor $prevDescriptor): Descriptor
    {
        $chain = $prevDescriptor?->getChain() ?? [];
        $chain[$stepName] = $elementId;

        return new Descriptor($chain);
    }

    public function getStep(string $stepName): string
    {
        return $this->getChain()[$stepName];
    }

    public function getLast(): int
    {
        return end($this->chain);
    }

    public function getChain(): array
    {
        return $this->chain;
    }
}
