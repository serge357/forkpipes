<?php

namespace Clavius\Pipes\Repo;

class ArrayRepo implements StoreRepoInterface
{
    protected array $vault = [];

    public function load(string $relativePath): mixed
    {
        return $this->vault[$relativePath];
    }

    public function store(string $relativePath, mixed $data): bool
    {
        $this->vault[$relativePath] = $data;

        return true;
    }

    public function delete(string $relativePath)
    {
        unset($this->vault[$relativePath]);
    }

    public function setRoot(string $path)
    {
    }
}
